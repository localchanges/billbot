'''
MIT License

Copyright (c) 2020 Local Changes Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
'''

# Native libraries
import datetime
import os

# Constant variables
sCONTAINING_DIR = os.path.split(__file__)[0]
sCSV_BASENAME = "csv"
sIMAGES_BASENAME = "images"
sBILLS_BASENAME = "bills"
sUNUSED_IMAGES_BASENAME = "unused"
sUSED_IMAGES_BASENAME = "used"
sSTAGING_BASENAME = "staging"
sUPLOADED_BASENAME = "uploaded"
sTEXTFILE_BASENAME = "instagram"
sPROCESSEDBILL_BASENAME = "output"
sIMAGE_EXT = ".jpg"
sTEXT_EXT = ".txt"


class BillbotBase(object):
    def __init__(self):
        self.sRootDir = sCONTAINING_DIR
        # self.sCurrentYear = "%s" % datetime.datetime.now().year

    def set_root_dir(self, sRootDir):
        """
        Sets the Root Dir is the Billbot root directory, which shall contain the "img" subdir
        """
        self.sRootDir = sRootDir

    def get_image_root_dir(self):
        """
        This represents the directory containing various types of image sub-directories

        Returns: Directory path
        """
        return os.path.join(self.sRootDir, sIMAGES_BASENAME)

    def get_staging_dir(self, sStateCode, sSession, sBillType):
        """
        This represents the directory of Bills which have been created but not uploaded

        Returns: Directory path
        """
        return os.path.join(self.sRootDir, sBILLS_BASENAME, sSTAGING_BASENAME, sStateCode, sSession, sBillType)

    def get_used_dir(self):
        """
        This represents the directory of images we've already used for Bill-making

        Returns: Directory path
        """
        return os.path.join(self.sRootDir, sIMAGES_BASENAME, sUSED_IMAGES_BASENAME)

    def get_background_image_glob_string(self):
        """
        These are images which will be used in the background of a Bill image

        Returns: Glob string
        """
        return os.path.join(self.sRootDir, sIMAGES_BASENAME, sUNUSED_IMAGES_BASENAME, "*" + sIMAGE_EXT)

    def get_uploaded_bills_dir(self, sStateCode, sSession, sBillType):
        """
        This directory represents images which have been uploaded to Social Media

        Returns: Path to directory
        """
        return os.path.join(self.sRootDir, sBILLS_BASENAME, sUPLOADED_BASENAME, sStateCode, sSession, sBillType)

    def get_staging_csv_dir(self, sStateCode, sSession):
        """
        This directory holds csv files for that are ready for batch instagram posting

        Returns: Path to directory
        """
        return os.path.join(self.sRootDir, sCSV_BASENAME, sSTAGING_BASENAME, sStateCode, sSession)

    def get_uploaded_csv_dir(self, sStateCode, sSession):
        """
        This directory holds csv files that have been uploaded

        Returns: Path to directory
        """
        return os.path.join(self.sRootDir, sCSV_BASENAME, sUPLOADED_BASENAME, sStateCode, sSession)