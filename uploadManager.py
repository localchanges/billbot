'''
MIT License

Copyright (c) 2020 Local Changes Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
'''

# Import native dependencies
import ftplib
import os

# Internal dependencies
import shutil
import time
from stat import ST_ATIME

import billbotBase
from billbotConfig import sFTP_HOST, sFTP_USER, sFTP_PASS, sFTP_ROOT


class UploadManager(billbotBase.BillbotBase):
    """
    Simple class for billbot FTP related tasks.
    Only a class so we can keep the one FTP instance for all operations.

    """
    def __init__(self):
        super(UploadManager, self).__init__()
        self.oFtp = ftplib.FTP(sFTP_HOST, sFTP_USER, sFTP_PASS)
        self.sFtpBillbotRoot = sFTP_ROOT

    def _convert_path(self, sPath):
        """
        Convert from paths provided by BillBotPaths to FTP friendly ones.

        Returns (str): String of converted path.

        """

        sPath = sPath.replace(self.sRootDir, self.sFtpBillbotRoot)
        sPath = sPath.replace('\\', '/')
        return sPath

    def get_uploaded_url(self, sPath):

        sFtpPath = self._convert_path(sPath)
        return "http:/" + sFtpPath # TODO: Make these constants/safer somewhere

    def upload_bill_img(self, sFilePath, bSetRemoteDir=False):
        """
        Upload a bill image to the ftp.
        Args:
            sFilePath (str): Filepath of the image ont he local billbot repo in the staging dir
            bSetRemoteDir (bool): If True, navigate to the correct folder on the FTP first.  If False, assumes you are
                                    in the right spot.

        Returns: None

        """
        sDir, sFileName = os.path.split(sFilePath)
        sRemoteDir = self.oFtp.pwd()
        if bSetRemoteDir:
            sRemoteDir = self._convert_path(sDir.replace(billbotBase.sSTAGING_BASENAME, billbotBase.sUPLOADED_BASENAME))
            self.oFtp.cwd(sRemoteDir)
        # Upload image to ftp
        sFileName = os.path.split(sFilePath)[-1]
        oFile = open(sFilePath, 'rb')
        self.oFtp.storbinary('STOR %s' % sFileName, oFile)
        oFile.close()
        print('Uploaded %s' % sFileName)
        sUrl = 'http://%s/%s' % (sRemoteDir[1:], sFileName)
        return sUrl

    def upload_files_in_dir(self, sDir):
        """
        Upload all .jpg files in a directory.
        Also copies from staging to uploaded.

        Args:
            sDir (str): Dir to operate on

        Returns: None

        """
        sFiles = [i for i in os.listdir(sDir) if os.path.splitext(i)[-1] == billbotBase.sIMAGE_EXT]
        if sFiles:

            # Navigate to the correct dir on the FTP
            # Path tricks - there is no 'staging' folder on the FTP, so if the path contains that
            # replace it with 'uploaded'
            self.oFtp.cwd(self._convert_path(sDir.replace(billbotBase.sSTAGING_BASENAME, billbotBase.sUPLOADED_BASENAME)))

            for sFileName in sFiles:
                self.upload_bill_img(os.path.join(sDir, sFileName))

                # Move to uploaded dir
                move_bill_to_uploaded(os.path.join(sDir, sFileName))


    def upload_files_in_staging(self, sBillType=None):
        """
        Uploads all files in the staging root dir, so all bill categories.
        Basically just operates on the contents of all dirs below sDir.

        Returns: None

        """
        if not sBillType:
            sBillCategoryDirs = [self.get_staging_dir(i) for i in os.listdir(self.get_staging_dir(''))]
        else:
            if type(sBillType) in [list, tuple]:
                sBillCategoryDirs = [self.get_staging_dir(i) for i in sBillType]
            else:
                sBillCategoryDirs = [self.get_staging_dir(sBillType)]

        for sDir in sBillCategoryDirs:
            self.upload_files_in_dir(sDir)

    def list_contents(self, sDir, sFilterExt=None):
        """
        List the contents of a folder on the FTP server.
        Args:
            sDir (str): Path of the dir to list, relative to server root where /img is.
            sFilterExt (str): Filter by file extension.

        Returns (list): List of FTP dir contents.

        """
        sContents = self.oFtp.nlst(sDir)
        if sFilterExt:
            sContents = [i for i in sContents if os.path.splitext(i)[-1] == sFilterExt]
        return sContents

    def list_uploaded_bills(self, sBillType=None):
        """
        List which bills have been uploaded to the FTP, by category.

        Args:
            sBillType (str): Optional arg for bill category.

        Returns (dict): List of uploaded bills by category.

        """
        if not sBillType:
            sBillCategoryDirs = [self.get_staging_dir(i) for i in os.listdir(self.get_staging_dir(''))]
        else:
            if type(sBillType) in [list, tuple]:
                sBillCategoryDirs = [self.get_staging_dir(i) for i in sBillType]
            else:
                sBillCategoryDirs = [self.get_staging_dir(sBillType)]

        dDirContents = {}
        for sDir in sBillCategoryDirs:
            dDirContents[os.path.split(sDir)[-1]] = self.list_contents(self._convert_path(sDir.replace(billbotBase.sSTAGING_BASENAME, billbotBase.sUPLOADED_BASENAME)))

        return dDirContents

    def quit(self):
        """
        Close the connection to the FTP server.

        Returns: None

        """
        self.oFtp.quit()


def move_bill_to_uploaded(sBillImagePath):

    # Move to uploaded dir
    sBillTextPath = sBillImagePath.replace('_%s%s' % (billbotBase.sPROCESSEDBILL_BASENAME,
                                                      billbotBase.sIMAGE_EXT),
                                           '_%s%s' % (billbotBase.sTEXTFILE_BASENAME,
                                                      billbotBase.sTEXT_EXT))
    statImg, statTxt = os.stat(sBillImagePath), os.stat(sBillTextPath)
    # Update timestamp for modified for easy dir sorting
    os.utime(sBillImagePath, (statImg[ST_ATIME], time.time()))
    os.utime(sBillTextPath, (statTxt[ST_ATIME], time.time()))

    sDstDir = os.path.dirname(sBillImagePath.replace(billbotBase.sSTAGING_BASENAME, billbotBase.sUPLOADED_BASENAME))
    if not os.path.exists(sDstDir):
        os.makedirs(sDstDir)

    shutil.move(sBillImagePath, os.path.join(sDstDir, os.path.basename(sBillImagePath)))
    shutil.move(sBillTextPath, os.path.join(sDstDir, os.path.basename(sBillTextPath)))