'''
MIT License

Copyright (c) 2020 Local Changes Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
'''

# Native dependencies
import base64

import sys
import bs4
import re
import os
import glob
import unicodedata
import platform
import csv
import datetime

# Python 2
if int(platform.python_version().split(".")[0]) < 3:
    from urllib2 import urlopen
# Python 3
else:
    from urllib.request import urlopen

from unidecode import unidecode
import pprint

# Third party dependencies
import pylegiscan.legiscan as legiscan
import pylegiscan.codes as codes
import numpy as np

# Internal dependencies
import billbotBase
import billbotConfig
import imageManager

# Constant variables
sSTATE_CODE = 'us'
sCONTAINING_DIR = os.path.split(__file__)[0]
sBILL_CATEGORIES = ['AB', 'HB', 'SB', 'S', 'H']


class BillbotManager(billbotBase.BillbotBase):
    """
    This is the main billBot class.  It handles interfacing with legiscanAPI, as well as imageManager.
    It's the one stop shop.  Does all file handling and image processing.

    """

    def __init__(self):
        super(BillbotManager, self).__init__()

        self.oLegiscan = legiscan.LegiScan(apikey=billbotConfig.sLEGISCAN_API_KEY)
        self.oImageManager = imageManager.ImageManager()
        self.dMasterBillStubs = None
        self.oCsvDictWriter = None

        self.state_code = None
        self.session = None
        self.set_root_dir(sCONTAINING_DIR)
        self.set_state_code(sSTATE_CODE)
        self.get_master_bill_stubs()

    def set_session(self, iSessionId=None):
        """
        Set the session of congress by legiscan session id.

        Args:
            iSessionId (int): legiscan session id.

        Returns: None

        """
        if not iSessionId:
            self.session = self.oLegiscan.get_session_list()[0]
        else:
            allSessions = self.oLegiscan.get_session_list()
            for session in allSessions:
                if session['session_id'] == iSessionId:
                    self.session = session
                    break

        print("Session set to %s." % self.session['name'])

    def set_state_code(self, sStateCode):
        """
        Set the state we want to get bills from.

        Args:
            sStateCode (str): Two letter state code.  See pylegiscan.codes.STATES

        Returns: None

        """
        self.state_code = sStateCode
        self.oLegiscan.state = sStateCode
        self.set_session()

        print("State code set to %s." % sStateCode)
        print("WARNING: You may want to run get_master_bill_stubs again to filter for existing staged or uploaded Bills.")

    def set_root_dir(self, sDir):
        """
        Because both BillBotHandle and ImageManager inherit BillBotPaths, and BillBotHandle
        instances ImageManager in it's __init__ have to overwrite set_root_dir() to also
        set the BillBotPaths instance in ImageManager.

        Args:
            sDir (str): Root billbot path.

        Returns: None

        """
        if os.path.exists(sDir):
            super(BillbotManager, self).set_root_dir(sDir)
            self.oImageManager.set_root_dir(sDir)
        else:
            raise Exception('Directory %s invalid')

        print("Root dir now set to %s." % sDir)
        print("WARNING: You may want to run get_master_bill_stubs again to filter for existing staged or uploaded Bills.")

    def get_master_bill_stubs(self, bFilterSeenBills=True, bFilterNonBills=True):
        """
        Populate the master list.  Billbot currently only cares about introduced bills, in name order.

        Args:
            bFilterSeenBills (bool): Trim the master list of already processed bills.
            bFilterNonBills (bool): Remove non-bills (Resolutions, etc)
        Returns: None

        """
        # Get em'
        self.dMasterBillStubs = self.oLegiscan.get_new_bill_stubs(session_id=self.session['session_id'])

        # Sort em'
        self.dMasterBillStubs.sort(key=self.get_bill_name_integer_from_stub)

        print("Found %s %s bills..." %(len(self.dMasterBillStubs), self.state_code))

        # Trim em'
        if bFilterSeenBills:
            iProcessedBillIds = self.get_processed_bill_ids()
            print("Removing %s processed %s bills..." % (len(iProcessedBillIds), self.state_code))
            self.dMasterBillStubs = list(filter(lambda d: d['bill_id'] not in iProcessedBillIds,
                                                self.dMasterBillStubs))
        # Filter em'
        if bFilterNonBills:
            iOldCount = len(self.dMasterBillStubs)
            self.dMasterBillStubs = list(filter(lambda d: self.get_bill_name_category_from_stub(d) in sBILL_CATEGORIES,
                                                self.dMasterBillStubs))
            print("Removing %s 'non bills'..." % (iOldCount - len(self.dMasterBillStubs)))

    def peek_next_bill_stub(self, key=None, offset=0):
        """
        Return the current bill stub, the first item in master list.

        Args:
            key (str): An optional dict key if only interested in specific info.

        Returns (dict/str): Bill or bill data field.

        """

        if key:
            return self.dMasterBillStubs[offset][key]
        return self.dMasterBillStubs[offset]

    def pop_next_bill_stub(self, key=None):
        """
        Return the next bill stub in the list by popping it.

        Args:
            key (str): Key to directly filter returned bill dict.

        Returns (dict/str): Bill or bill data field.

        """

        if key:
            return self.dMasterBillStubs.pop(0)[key]
        return self.dMasterBillStubs.pop(0)

    def process_next_bill_stub(self):
        """
        Process the next bill stub.  Gets the bill data, creates the image and comment files.

        Returns (tuple): Bill stub dict, processed bill image file path, text of the processed bill.

        """

        dBillStub = self.pop_next_bill_stub()
        dProcessedBill = self.process_bill_stub_by_id(dBillStub['bill_id'])
        sImgFilePath, sPostText = self.oImageManager.make_billbot_content(dProcessedBill)
        return dBillStub, sImgFilePath, sPostText

    def process_bills(self, iStopAt=None):
        """
        Process bills until told to stop, or we run out of data.

        Returns: None

        """
        if not iStopAt:
            iStopAt = len(self.dMasterBillStubs)
        i = 0
        while self.peek_next_bill_stub() and self.oImageManager.get_random_background_image_path() and i < iStopAt:
            self.process_next_bill_stub()
            i += 1

    def generate_bills_csv(self, sBillType, iStopAt=None):
        """
        Create a .csv file of processed bills.  This was intended for use in bulk uploading applications.

        NOTE:
            Will always only be 1 CSV file in the staging dir.
            If it sees one there already, then append the bills to it.
            We manually move it to uploaded, indicating that we should start another new file.

        Args:
            iStopAt (int): Number of bills to process.
            sBillType (str): Type of bill to process, corresponding to a staging folder, IE AB, SB, AR etc.

        Returns: None

        """
        def generate_csv_file_name():
            now = datetime.datetime.now()
            return '%s_%s.csv' % (now.date(), np.random.randint(9999))

        if not iStopAt:
            iStopAt = len(self.dMasterBillStubs)

        sStateCsvStagingDir = self.get_staging_csv_dir(self.state_code)

        if not os.path.exists(sStateCsvStagingDir):
            os.makedirs(sStateCsvStagingDir)
            print('Could not find existing csv staging dir, creating it: %s' % sStateCsvStagingDir)

        sCsvFiles = [i for i in os.listdir(sStateCsvStagingDir) if os.path.splitext(i)[-1] == '.csv']

        bWriteCsvHeader = True
        if sCsvFiles:
            if len(sCsvFiles) > 1:
                print('Warning!  Multiple csv files fund in dir %s.  There can only be one.')
                return
            else:
                sCsvFilePath = os.path.join(sStateCsvStagingDir, sCsvFiles[0])
                bWriteCsvHeader = False
        else:
            sCsvFilePath = os.path.join(sStateCsvStagingDir, generate_csv_file_name())

        sFieldNames = ['text',
                       'year',
                       'month',
                       'day',
                       'hour',
                       'minute',
                       'image url',
                       'link']

        with open(sCsvFilePath, 'w') as oFile:

            oDateNow = datetime.datetime.now()

            oCsvWriter = csv.writer(oFile)
            if bWriteCsvHeader:
                oCsvWriter.writerow(sFieldNames)

            sUploadedBillsDir = self.get_uploaded_bills_dir(self.state_code, sBillType)
            print("Finding Uploaded Bills in dir: " + sUploadedBillsDir)

            assert os.path.exists(sUploadedBillsDir), ("ERROR: Uploaded Bills dir does not exist %s" % sUploadedBillsDir)

            sImgFilesBasenames = os.listdir(sUploadedBillsDir)
            sImgFilesBasenames = list(filter(lambda s: s.endswith(billbotBase.sIMAGE_EXT), sImgFilesBasenames)) # Jpgs only

            for sImgFileBasename in sImgFilesBasenames[:iStopAt]:
                oDateNow += datetime.timedelta(0, 0, 0, 0, 90)  # 90 minutes
                sImgFilePath = os.path.join(sUploadedBillsDir, sImgFileBasename)
                sBillTextFile = sImgFilePath.replace('_%s%s' % (billbotBase.sPROCESSEDBILL_BASENAME,
                                                    billbotBase.sIMAGE_EXT),
                                                    '_%s%s' % (billbotBase.sTEXTFILE_BASENAME,
                                                    billbotBase.sTEXT_EXT))
                sBillText = self.get_bill_text_from_file(sBillTextFile)
                oCsvWriter.writerow([sBillText,
                                     oDateNow.year,
                                     oDateNow.month,
                                     oDateNow.day,
                                     oDateNow.hour,
                                     oDateNow.minute])

    def get_random_bill_stubs(self, iNumber):
        """
        Gets a random amount of bill stubs for testing.

        Args:
            iNumber (int): Number of bill stubs to grab.

        Returns (dict): Bill stubs.

        """

        random_indices = np.random.randint(0, len(self.dMasterBillStubs), iNumber)
        return [self.dMasterBillStubs[i] for i in random_indices]

    def clean_unicode(self, sUnicode):

        return unicodedata.normalize('NFKD', sUnicode).encode('ascii', 'ignore').strip().decode("utf-8")

    def url_exists(self, sUrl, sTestString=""):
        """
        Check if a url exists.

        Args:
            sUrl (str): Url to test.
            sTestString:

        Returns (bool): True if it does in fact exist.

        """

        uf = urlopen(sUrl)
        try:
            sBody = uf.read()
            if (sTestString in sBody):
                return True
        except:
            return False

        return False

    def process_bill_stub_by_id(self, iBillId):
        """
        Format bill for input to ImageManager.makeImage().

        Args:
            bill_ids: list of bill ids.

        Returns (dict): Dict of relevant bill data for downstream billbot processing.  Note this id different
                        from the bill stub dict pylegiscan provides.

        """

        dRawBill = self.oLegiscan.get_bill(bill_id=iBillId)

        print ("Processing %s (%s)..." % (dRawBill['bill_number'], dRawBill['bill_id']))

        sSubject, sDigestText = self.format_bill_digest_text(dRawBill['texts'][0]['doc_id'])

        sSponsorIds = [(str)(dSponsor['people_id']) for dSponsor in dRawBill['sponsors']]

        sTwitterHandles = []
        sInstagramHandles = []
        sFacebookHandles = []
        dSocialMedias = [self.oLegiscan.find_social_media(s) for s in sSponsorIds]
        for d in dSocialMedias:
            if (d[codes.SOCIAL_MEDIA_FACEBOOK] != None):
                sFacebookHandles.append(d[codes.SOCIAL_MEDIA_FACEBOOK])
            if (d[codes.SOCIAL_MEDIA_INSTAGRAM] != None):
                sInstagramHandles.append(d[codes.SOCIAL_MEDIA_INSTAGRAM])
            if (d[codes.SOCIAL_MEDIA_TWITTER] != None):
                sTwitterHandles.append(d[codes.SOCIAL_MEDIA_TWITTER])

        # dIngestedBill = dict(title=self.clean_unicode(dRawBill['title']),
        #                      state_code=dRawBill['state'].lower(),
        #                      bill_number=self.clean_unicode(dRawBill['bill_number']),
        #                      bill_id=dRawBill['bill_id'],
        #                      subject=re.sub("\n\s+", "\n", sSubject),  # remove \n\t\t\t\t\t nonsense...
        #                      date_introduced=self.clean_unicode(dRawBill['history'][0]['date']),
        #                      digest_text=sDigestText,
        #                      url=dRawBill['url'],
        #                      sponsors=[self.format_sponsor(s) for s in sSponsorIds],
        #                      sponsors_twitter=sTwitterHandles,
        #                      sponsors_instagram=sInstagramHandles,
        #                      sponsors_facebook=sFacebookHandles
        #                      )

        dRawBill.update(dict(title=self.clean_unicode(dRawBill['title']),
                             state_code_lower=dRawBill['state'].lower(),
                             bill_number=self.clean_unicode(dRawBill['bill_number']),
                             subject=re.sub("\n\s+", "\n", sSubject),  # remove \n\t\t\t\t\t nonsense...
                             date_introduced=self.clean_unicode(dRawBill['history'][0]['date']),
                             digest_text=sDigestText,
                             sponsors_string=[self.format_sponsor(s) for s in sSponsorIds],
                             sponsors_twitter=sTwitterHandles,
                             sponsors_instagram=sInstagramHandles,
                             sponsors_facebook=sFacebookHandles,
                             ))

        return dRawBill

    def get_billbot_root_dir_from_output_path(self, sImgPath):
        """
        Get the billbot root path from a processed bill image path.

        Args:
            sImgPath (str): Bill path.

        Returns (str): Billbot root dir.

        """

        sTypeDir = os.path.dirname(sImgPath)
        sYearDir = os.path.dirname(sTypeDir)
        sStateDir = os.path.dirname(sYearDir)
        sStagingDir = os.path.dirname(sStateDir)
        sBillsDir = os.path.dirname(sStagingDir)
        return os.path.dirname(sBillsDir)

    def replace_bill_image(self, sOutputPath):
        """
        Replaces an undesired bill by re doing the process for it.  This grabs a new random image
        in effect swapping the bill image.

        Args:
            sOutputPath (str): Bill path.

        Returns (tuple): Path for the processed bill, text for the bill post.

        """
        # Get bill dict
        dResults = self.oLegiscan.search(bill_number=os.path.basename(sOutputPath).split("_")[2])['results']
        dBillStub = dResults[0]
        dProcessedBill = self.process_bill_stub_by_id(dBillStub['bill_id'])

        self.set_root_dir(self.get_billbot_root_dir_from_output_path(sOutputPath))
        sImgFilePath, sPostText = self.oImageManager.make_billbot_content(dProcessedBill)
        return sImgFilePath, sPostText

    def format_sponsor(self, people_id):
        """
        Prepare a string representing a bill sponsor from the pylegiscan people_id.
        Args:
            people_id (int): pylegiscan people_id corresponding to the sponsor.

        Returns (str): Sponsor name string.

        """

        dSponsor = self.oLegiscan.get_sponsor(people_id=people_id)
        sSponsorStr = '%s. %s' % (dSponsor['role'], dSponsor['name'])
        sParty = dSponsor['party']
        if sParty:
            sSponsorStr += ' (%s)' % sParty
        return sSponsorStr

    def format_bill_digest_text(self, doc_id):
        """
        Prepare a tuple of the bill title, and digest text from the pylegiscan doc_id.
        Args:
            doc_id (int): pylegiscan doc_id of the corresponding bill.

        Returns (tuple): Bill title, and bill description.

        """

        # Get text of bill, it's encoded
        text = self.oLegiscan.get_bill_text(doc_id)

        # Decode to html
        html = base64.b64decode(text['doc'])

        # Use html parser to navigate
        bs = bs4.BeautifulSoup(html, features='html.parser')
        subject = bs.find(id=['subject'])
        if(subject == None):
            subject_str = ""
        else:
            subject_str = subject.string.strip()

        digest_text = bs.find(id=['digesttext'])
        if digest_text is None or digest_text.div is None or digest_text.div.string is None:
            # Most likely a resolution, so fall back to resolved text
            resolutions = bs.select('[class=Resolved]')
            if resolutions:
                digest_text_str = ''
                for i, resolution in enumerate(resolutions):
                    for child in resolution.children:
                        digest_text_str += child.text + '\n'
            else:
                digest_text_str = "(No digest text provided.)"
        else:
            digest_text_str = digest_text.div.string.strip()

        digest_text_str = unidecode(digest_text_str)

        return subject_str, digest_text_str

    def get_processed_bill_ids(self):
        """
        Get the processed bills by combining the lists of files in both staging and uploaded.
        Used to trim the master list.

        Returns (list): List of bill ids.

        """

        sStagedGlobPath = os.path.join(self.get_staging_dir(self.state_code, self.session['name'], "*"), "*" + billbotBase.sIMAGE_EXT)
        sStagedImagePaths = glob.glob(sStagedGlobPath)
        print("Found %s processed bills in path %s." % ((len(sStagedImagePaths), sStagedGlobPath)))
        iStagedBillIds = (os.path.basename(s) for s in sStagedImagePaths)
        iStagedBillIds = [int(s.split("_")[3]) for s in iStagedBillIds]
        iStagedBillIds = list(set(iStagedBillIds))
        iStagedBillIds.sort()

        sUploadedGlobPath = os.path.join(self.get_uploaded_bills_dir(self.state_code, self.session['name'], "*"), "*" + billbotBase.sIMAGE_EXT)
        sUploadedImagePaths = glob.glob(sUploadedGlobPath)
        print("Found %s processed bills in path %s." % ((len(sUploadedImagePaths), sUploadedGlobPath)))
        iUploadedBillIds = (os.path.basename(s) for s in sUploadedImagePaths)
        iUploadedBillIds = [int(s.split("_")[3]) for s in iUploadedBillIds]
        iUploadedBillIds = list(set(iUploadedBillIds))
        iUploadedBillIds.sort()

        # Combine Staged and Uploaded bills
        iStagedBillIds.extend(iUploadedBillIds)
        iStagedBillIds = list(set(iStagedBillIds))
        iStagedBillIds.sort()

        return iStagedBillIds

    # def get_processed_bill_names(self, sPath):
    #
    #     if not os.path.exists(sPath):
    #         raise Exception('Path %s does not exist' % sPath)
    #     sSeenBillNames = os.listdir(sPath)
    #     sSeenBillNames = [s.split("_")[2] for s in sSeenBillNames]
    #     sSeenBillNames = list(set(sSeenBillNames))
    #     sSeenBillNames.sort()
    #     return sSeenBillNames

    # def get_processed_bill_names_by_category(self, sPath):
    #     """
    #     Get the processed bills, organized by category.
    #
    #     Args:
    #         sPath (str): Path for the processed bills.
    #
    #     Returns (dict):
    #
    #     """
    #
    #     if not os.path.exists(sPath):
    #         raise Exception('Path %s does not exist' % sPath)
    #     sSeenBillFiles = os.listdir(sPath)
    #     sSeenBillNames = [s.split("_")[2] for s in sSeenBillFiles]
    #     sSeenBillNames = list(set(sSeenBillNames))
    #     sSeenBillNames.sort()
    #
    #     sBillCategories = [self.get_bill_name_category(i) for i in sSeenBillNames]
    #     splitHere = np.where(sBillCategories != np.roll(sBillCategories, -1))[0]
    #
    #     # Split bills by name category using index slices
    #     dBillCategories = {}
    #     for i in range(len(splitHere))[1:]:
    #         dBillCategories[sBillCategories[splitHere[i]]] = sSeenBillNames[splitHere[i-1]:splitHere[i]]
    #
    #     # Sort properly inside the categories
    #     for key in dBillCategories.keys():
    #         dBillCategories[key].sort(key=self.get_bill_name_integer)
    #
    #     return dBillCategories

    def sort_bills_by_date(self, bills):
        """
        Sorts bill stubs by date.

        Args:
            bills (list): List of bill stubs.

        Returns (list): List of indices that would order bills by date.

        """

        dates = np.array([i['history'][0]['date'] for i in bills])
        order = np.argsort(dates)
        return order

    def get_bill_category_and_integer(self, sBillNumber):
        """
        Get the bill category and name integer.

        Args:
            sBillNumber (str):

        Returns (tuple): Bill category, bill name integer.

        """
        groups = re.match("([A-Z]+)+(\d+)", sBillNumber).groups()
        return groups[0], int(groups[1])

    def get_bill_name_category_from_stub(self, dBillStub):
        """
        Get the bill name category from a bill stub.

        Args:
            dBillStub (dict):

        Returns (str): Bill name category, IE: AB for 'AB65'

        """
        return self.get_bill_category_and_integer(dBillStub['number'])[0]

    def get_bill_name_category(self, sBillNumber):
        """
        Get the bill name category from it's number.

        Args:
            sBillNumber (str):

        Returns (str): Bill name category, IE: AB for 'AB65'

        """
        return self.get_bill_category_and_integer(sBillNumber)[0]

    def get_bill_name_integer_from_stub(self, dBillStub):
        """
        Get the bill name integer from a bill stub.

        Args:
            dBillStub (dict):

        Returns (int): Bill name integer, IE: 65 for 'AB65'

        """
        return self.get_bill_category_and_integer(dBillStub['number'])[1]

    def get_bill_name_integer(self, sBillNumber):
        """
        Get the bill name integer from it's number.

        Args:
            sBillNumber (str):

        Returns (int): Bill name integer, IE: 65 for 'AB65'

        """
        return self.get_bill_category_and_integer(sBillNumber)[1]

    def get_bill_text_from_file(self, sTextFile):
        """
        Get the bill text from a text file.

        Args:
            sTextFile (str): Path to the bill text file.

        Returns (str): Contents of the text file.

        """

        oFile = open(sTextFile, 'r')
        sContents = oFile.read()
        oFile.close()

        return sContents

    def print_bill(self, dBill):
        """
        Pretty prints a bill.

        Args:
            dBill (dict): Bill dict to print.

        Returns: None

        """
        pprint.pprint(dBill)


def get_bill_data_from_file_name(sFile):

    sFile = os.path.split(sFile)[-1]
    iYear, sBillNumber, iBillId, _ = sFile.split('_')
    return iYear, sBillNumber, iBillId


if __name__ == "__main__":

    oBillbot = BillbotManager()
    oBillbot.set_state_code(sys.argv[1])
    oBillbot.get_master_bill_stubs()

    if len(sys.argv) > 2:
        print("Creating up to %s bills for the state of %s." % (sys.argv[1].lower(), sys.argv[2]))
        oBillbot.process_bills(sys.argv[2])
    else:
        print("Creating bills for the state of %s." % (sys.argv[2].lower()))
        oBillbot.process_bills()