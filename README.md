# Billbot

Billbot is a Python3 library to aid in the creation of images which summarize bills introduced by state legislatures.

### Sample Output

![Sample Bill](http://wheatpastedjpeg.shop/billbot/us_2019_SB528_1205699_output-500.jpg)

## Setup

### Dependencies

Use Python 3's pip toolset to install Billbot's dependencies.

```bash
python -m pip install Pillow
python -m pip install bs4
python -m pip install unidecode
python -m pip install requests
python -m pip install numpy
python -m pip install python-pixabay
```

You will also need to install [Image Magick](https://imagemagick.org/script/download.php) as well as our chose default font, PrimaSerif_BT_Roman.ttf (this font can be found in the repository).

### Config

You will need a Legiscan API Key (and optionally a Pixabay API Key), both of which must be populated in the billbotConfig.py file.


## Usage

```python
import billbotManager

oBillbot = billbotManager.BillbotManager()
oBillbot.set_state_code('ca') # In this example, using 'ca' for California.
oBillbot.process_bills(iStopAt=1) # If you're just testing, stop after 1 bill.
```

## Contributing
Pull requests are welcome. Problems we would most like to solve are:
- Automation of media upload to Instagram
- Robust solution to finding the Social Media accounts of legislators (we are open to commits of a brute-force nature to our JSON file as well!)
- Creating OS agnostic versions of our batch files


## Support
hello@localchanges.org

## License
[MIT](https://choosealicense.com/licenses/mit/)