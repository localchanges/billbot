'''
MIT License

Copyright (c) 2020 Local Changes Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
'''

# Python Native Dependencies
import os
import glob
import shutil
import re
from random import shuffle

# Third Party Dependencies
# Note: This is Pillow, not PIL
from PIL import Image

# Internal Dependencies
import billbotBase
from pylegiscan.codes import STATE_NAMES, STATE_CAPITALS

# Class constants
sCONTAINING_DIR = os.path.split(__file__)[0]
sRESOLUTION = "2048x2048"
sFONT_NAME = "PrimaSerif BT"

# Template constants
sBILL_NAME_TEMPLATE = "BILL_NAME"
sDATE_INTRODUCED_TEMPLATE = "DATE_INTRODUCED"
sBILL_TITLE_TEMPLATE = "BILL_TITLE"
sSPONSOR_NAME_TEMPLATE = "SPONSOR_NAME"
sPANGO_CONTENT_TEMPLATE = """<markup>

<span fgcolor="white" bgcolor="#000000BF" size="204800" font_desc="%s">%s</span>

<span fgcolor="white" bgcolor="#000000BF" size="51200" font_desc="%s"><i>Introduced %s</i></span>

<span fgcolor="white" bgcolor="#000000BF" size="102400" font_desc="%s">%s</span>

<span fgcolor="white" bgcolor="#000000BF" size="51200" font_desc="%s"><i>%s</i></span>

</markup>
""" % (sFONT_NAME, sBILL_NAME_TEMPLATE,
       sFONT_NAME, sDATE_INTRODUCED_TEMPLATE,
       sFONT_NAME, sBILL_TITLE_TEMPLATE,
       sFONT_NAME, sSPONSOR_NAME_TEMPLATE)

# Max number of legislators to list as attached to a bill
iMAX_SPONSORS_TO_DISPLAY = 8
iMAX_SPONSORS_TO_TAG = 20

# The maximum amount of people that you can tag on Instagram is 20 people.
# Furthermore, the maximum number of hashtags that you can use in a single post is 30.

class ImageManager(billbotBase.BillbotBase):
    """
    This class is responsible for the creation of Billbot media content
    """
    def __init__(self):
        super(ImageManager, self).__init__()

        self.set_root_dir(sCONTAINING_DIR)

    def get_random_background_image_path(self):
        """
        This function shuffles the images in ./img and picks one.

        Returns: Image path
        """
        sAllImagePaths = glob.glob(self.get_background_image_glob_string())
        if len(sAllImagePaths) == 0:
            return None

        shuffle(sAllImagePaths)
        return sAllImagePaths[0]


    def stage_background_image(self, sBillBasename):
        """
        This function shuffles the images in ./img and picks one.
        If the image it picks appears to have been used before,
        it will move it to the ./img/duplicate dir and pick a new one.
        Otherwise, it moves it to the ./img/used dir and returns the
        path of the newly moved image.

        Returns:
        Image path in ./img/used dir.
        """

        sImgSrc = self.get_random_background_image_path()
        if not sImgSrc:
            raise Exception("Out of source jpg files: %s" % self.get_background_image_glob_string())

        sUsedDir = self.get_used_dir()
        if not os.path.exists(sUsedDir):
            os.makedirs(sUsedDir)

        sImgDst = os.path.join(sUsedDir, sBillBasename + "_" + os.path.basename(sImgSrc))

        # This should be an impossible case, if you're staying organized.
        while os.path.exists(sImgDst):
            print("\nWarning: %s already exists. Appending '-2' to new file path." % sImgDst)
            sImgDstMain, sImgDstExt = os.path.splitext(sImgDst)
            sImgDst = sImgDstMain + "-2" + sImgDstExt

        print("\nRunning:\nmove %s %s" % (sImgSrc, sImgDst))
        shutil.move(sImgSrc, sImgDst)

        return sImgDst


    def make_billbot_content(self, dIngestedBill):
        """
        This function will take processed bill data and turn it into a comped "billbot" image and
        a corresponding "billbot" text file, containing social media content.

        It is "wet" in that it calls stageBackgroundImage, which moves an image from ./img to ./img/used
        as well as creates a txt and jpg file in ./img/output

        Args:
            dIngestedBill (dict): a processed bill dictionary (expanded from a stub)

        Returns (str):  output path and text file output path
        """

        # Prepare the parameters of the bill
        sBillName = self.formatBillName(dIngestedBill)
        print("\nCreating Image for: %s" % sBillName)
        sSponsors = self.format_sponsors(dIngestedBill)
        sDate = dIngestedBill['date_introduced']
        sYear = sDate.split('-')[0]
        sBillId = str(dIngestedBill['bill_id'])
        sBillTitleText = dIngestedBill['title']
        sBasename = "%s_%s_%s_%s" % (dIngestedBill['state_code_lower'], sYear, sBillName.replace(" ", ""), sBillId)
        sInstagramComment = self.format_instagram_comment(dIngestedBill)

        # Prepare the output directory
        sSubdirPath = self.get_staging_dir(dIngestedBill['state_code_lower'],
                                           dIngestedBill['session']['year_start'],
                                           sBillName.split(" ")[0])
        if not os.path.exists(sSubdirPath):
            os.makedirs(sSubdirPath)

        # Stage an image by selecting one and moving it to the used directory
        sSrcImage = self.stage_background_image(sBasename)
        oImage = Image.open(sSrcImage, 'r')
        iSrcWidth = oImage.width
        iSrcHeight = oImage.height
        oImage.close()

        # Create the cropped image
        sCroppedImagePath = os.path.join(sSubdirPath, "%s_cropped.jpg" % sBasename)
        iMinDim = min(iSrcWidth, iSrcHeight)
        sCropCmd = 'magick convert "%s" -gravity center -crop %sx%s+0+0 %s' % (
            sSrcImage, iMinDim, iMinDim, sCroppedImagePath)

        print("\nRunning:\n" + sCropCmd)
        os.system(sCropCmd)

        # Resize the cropped image to a square of resonable dimension
        sResizedImagePath = os.path.join(sSubdirPath, "%s_resized.jpg" % sBasename)
        sResizeCmd = 'magick convert %s -resize %s %s' % (sCroppedImagePath, sRESOLUTION, sResizedImagePath)

        print("\nRunning:\n" + sResizeCmd)
        os.system(sResizeCmd)

        # Prepare the pango file
        sPangoPath = os.path.join(sSubdirPath, "%s_pango.txt" % sBasename)
        oFile = open(sPangoPath, 'w')
        sPangoContent = sPANGO_CONTENT_TEMPLATE.replace(sBILL_NAME_TEMPLATE, sBillName).replace(sDATE_INTRODUCED_TEMPLATE, sDate).replace(
            sBILL_TITLE_TEMPLATE, sBillTitleText).replace(sSPONSOR_NAME_TEMPLATE, sSponsors)
        oFile.write(sPangoContent)
        oFile.close()

        # Apply the pango file to the image
        sOutputPath = os.path.join(sSubdirPath, "%s_output.jpg" % sBasename)

        sCompCommand = "magick convert %s -size 2048x -background transparent -gravity center pango:@%s -composite %s" % (
            sResizedImagePath, sPangoPath, sOutputPath)

        print("\nRunning:\n" + sCompCommand)
        os.system(sCompCommand)

        # Write the comment text file
        sInstagramCommentPath = os.path.join(sSubdirPath, "%s_instagram.txt" % sBasename)
        oFile = open(sInstagramCommentPath, 'w')
        oFile.write(sInstagramComment)
        oFile.close()

        # Clean up temporary files
        print ("\nRemoving temp files...")
        os.remove(sResizedImagePath)
        os.remove(sCroppedImagePath)
        os.remove(sPangoPath)

        print ("\n%s complete." % sBillName)

        return sOutputPath, sInstagramComment

    def format_instagram_comment(self, dIngestedBill):

        sComment = dIngestedBill['digest_text']
        sComment += "\n\n"
        sComment += dIngestedBill['url']

        sUsers = " ".join(dIngestedBill['sponsors_instagram'][:iMAX_SPONSORS_TO_TAG])
        if (sUsers != ""):

            if(len(dIngestedBill['sponsors_instagram']) > iMAX_SPONSORS_TO_TAG):
                sUsers += " ..."

            sComment += "\n\n" + sUsers

        # our tag game is weak. removing this for now.
        # sTags = (sUsers + " " + formatTags(dIngestedBill)).strip()
        sStateCode = dIngestedBill['state_code_lower'] or ''
        sStateName = ''
        sStateCapital = ''
        if sStateCode:
            sStateName = STATE_NAMES[sStateCode.upper()]
        if sStateName:
            sStateCapital = STATE_CAPITALS[sStateName]
        sComment += "\n\n" + "#{0}Legislature #Legislation #Legal #Bill #Law #Democrat #Republican #Democracy #Vote #Voting #{1} #Senate #Assembly #StateLaw #Ballot #Activist #Activism #CivicHacking #stayinformed #getinformation #{2} #Government #Govt #BillBot".format(sStateCode,
                                                                                                                                                                                                                                                                      sStateName,
                                                                                                                                                                                                                                                                      sStateCapital)
        # Add the bill number as hashtag
        sComment += ' #%s' % dIngestedBill['bill_number']

        return sComment

    def formatBillName(self, dIngestedBill):

        kind, num = re.match("([A-Z]+)(\d+)", dIngestedBill['bill_number']).groups()
        return "%s %s" % (kind, num)

    def format_sponsors(self, dIngestedBill):

        if (len(dIngestedBill['sponsors_string']) > iMAX_SPONSORS_TO_DISPLAY):
            return ", ".join(dIngestedBill['sponsors_string'][:iMAX_SPONSORS_TO_DISPLAY]) + "..."
        return ", ".join(dIngestedBill['sponsors_string'])
