'''
MIT License

Copyright (c) 2020 Local Changes Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
'''

'''
pip install python-pixabay
Requries Python3 due to dependency on ABC module (abstract base classes).
'''

# Native dependencies
import os
import sys
import requests

# Third party dependencies
from pixabay import Image

# Internal Dependencies
from billbotConfig import sPIXABAY_API_KEY, sSTOCK_PHOTOGRAPHY_SAVE_DIR

# Constant variables
iMAX_PER_PAGE = 200
iMIN_PER_PAGE = 10
iMIN_IMAGE_DIMENSION = 1500

def get_save_path(d):

    if not os.path.exists(sSTOCK_PHOTOGRAPHY_SAVE_DIR):
        os.makedirs(sSTOCK_PHOTOGRAPHY_SAVE_DIR)

    sBasename = "pixabay_%s_%s%s" % (d['user'], d['id'], os.path.splitext(d['largeImageURL'])[-1])
    return os.path.join(sSTOCK_PHOTOGRAPHY_SAVE_DIR, sBasename)

def get_images_from_keyword(keyword, count):
    # image operations
    image = Image(sPIXABAY_API_KEY)

    # default image search
    image.search()

    iterations = count // iMAX_PER_PAGE
    if (iterations * iMAX_PER_PAGE < count):
        iterations += 1

    for page in range(0, iterations):

        perPage = iMAX_PER_PAGE
        if(page == iterations-1):
            perPage = max(iMIN_PER_PAGE, count % iMAX_PER_PAGE)

        # custom image search
        ims = image.search(q=keyword,
        image_type='photo',
        safesearch='true',
        order='latest',
        page=(page + 1),
        per_page=perPage)

        for d in ims['hits']:
            srcPath = d['largeImageURL']
            dstPath = get_save_path(d)
            if(os.path.exists(dstPath)):
                print("Already downloaded.")
            elif(d['imageWidth'] < iMIN_IMAGE_DIMENSION or d['imageHeight'] < iMIN_IMAGE_DIMENSION):
                print("Too small.");
            else:
                print("Downloading: %s" % srcPath)
                r = requests.get(srcPath, stream=True)
                if r.status_code == 200:
                    with open(dstPath, 'wb') as f:
                        for chunk in r:
                            f.write(chunk)


if __name__ == "__main__":

    get_images_from_keyword(sys.argv[1], sys.argv[2])